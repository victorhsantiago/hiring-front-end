import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatCardModule, MatInputModule, MatOptionModule, MatSelectModule, MatGridListModule, MatFormFieldModule } from '@angular/material';

import { AppComponent } from './app.component';
import { SearchUserComponent } from './search-user/search-user.component';
import { SearchUserService } from './search-user/search-user.service';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    SearchUserComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatGridListModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    HttpModule
  ],
  providers: [
    SearchUserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
