import { Component, OnInit } from '@angular/core';
import { SearchUserService } from './search-user.service';

@Component({
	selector: 'app-search-user',
	templateUrl: './search-user.component.html',
	styleUrls: ['./search-user.component.css']
})
export class SearchUserComponent implements OnInit {

	user: string //
	results: any[] = [] //this will hold the data coming from the service
	selected: boolean = false //flag to check if the user is selected or not
	selectedUser: any //presents Selected user details
	error_text: string //erro reporting


	constructor(private searchService: SearchUserService) { }

	ngOnInit() { }

	// searching by user
	search(user: string) {
		this.selected = false
		this.error_text = ''
		if (user) {
			this.user = user
			this.searchService.searchUser(user).subscribe(
				users => {
					this.results = users
					console.log(users)
				},
				error => {
					this.results = []
					this.error_text = "Sorry! No users found. Try again"
					console.error(error)
				}
			)
		}
	}

	//showing user details
	getDetails(user: string) {
		this.searchService.getUserDetail(user).subscribe(
			userDetails => {
				this.selectedUser = userDetails
				this.selected = true
				console.log(userDetails)
			},
			error => {
				this.selected = false
				console.error(error)
			}
		)
	}

}
