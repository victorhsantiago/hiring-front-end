import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/';

@Injectable({
    providedIn: 'root'
})
export class SearchUserService {

    private searchUsersUrl = "https://api.github.com/search/users?q="
    private getUserDetaisUrl = 'https://api.github.com/users/'

    constructor(private http: Http) { }

    //search user on Github
    searchUser(user: string) {
        let url
        if (user) {
            url = `${this.searchUsersUrl}user:${user}`

            return this.http.get(url)
                .pipe(map(this.extractData))
                .pipe(catchError(this.handleError))
        }
    }

    //get user details on Github
    getUserDetail(user: string) {
        let url
        if (user) {
            url = `${this.getUserDetaisUrl}${user}`

            return this.http.get(url)
                .pipe(map((res: Response) => res.json()))
                .pipe(catchError(this.handleError))

        }
    }

    //convert search result in a json object
    private extractData(res: Response) {
        let body = res.json();
        return body.items || {}
    }

    //generic error treatment
    private handleError(error: Response | any) {
        let errMsg: string
        if (error instanceof Response) {
            const body = error.json() || ''
            const err = body.error || JSON.stringify(body)
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`
        } else {
            errMsg = error.message ? error.message : error.toString()
        }
        console.log(errMsg)
        return Observable.throw(errMsg);

    }
}
